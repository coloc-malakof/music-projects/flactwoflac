#!/usr/bin/env sh
#################################################
# Authors
#   Coloc Malakof <colocmalakof@gmail.com>
# Documentation
#   https://gitlab.com/colocmalakof/processflac
#################################################

# Exit immediately if a command exits with a non-zero exit status
set -e

# Treat unset variables as an error when substituting
set -u

# Constants
HOME_DIRECTORY="/processflac"

CONFIG_DIRECTORY="${HOME_DIRECTORY}/config"
LOGS_DIRECTORY="${HOME_DIRECTORY}/logs"
TOIMPORT_DIRECTORY="${HOME_DIRECTORY}/toprocess"

echo "Moving the processflac' config file to ${CONFIG_DIRECTORY} if needed..."
if [[ ! -f "${CONFIG_DIRECTORY}/processflac.yaml" ]]; then
    cp "/etc/processflac/config.yaml" "${CONFIG_DIRECTORY}/processflac.yaml"
fi

echo "Starting processflac..."
processflac --config="${CONFIG_DIRECTORY}/processflac.yaml"
