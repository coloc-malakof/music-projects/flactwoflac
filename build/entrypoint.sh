#!/usr/bin/env sh
#################################################
# Authors
#   Coloc Malakof <colocmalakof@gmail.com>
# Documentation
#   https://gitlab.com/colocmalakof/processflac
#################################################

# Exit immediately if a command exits with a non-zero exit status
set -e

# Treat unset variables as an error when substituting
set -u

# Constants
CONTAINER_USER="processflac"
CONTAINER_HOME="/processflac"
CONTAINER_GROUP="processflac"

echo "Creating group..."
addgroup --gid "${GID}" --system "${CONTAINER_GROUP}"

echo "Creating user..."
adduser --uid "${UID}" --system --home "${CONTAINER_HOME}" "${CONTAINER_USER}"

echo "Add user to groups..."
addgroup "${CONTAINER_USER}" "${CONTAINER_GROUP}"

echo "Creating volumes directories..."
mkdir --parent "${CONTAINER_HOME}/config"
mkdir --parent "${CONTAINER_HOME}/logs"
mkdir --parent "${CONTAINER_HOME}/toprocess"

echo "Setting permissions..."
chown -R "${CONTAINER_USER}":"${CONTAINER_GROUP}" "${CONTAINER_HOME}/config"
chown -R "${CONTAINER_USER}":"${CONTAINER_GROUP}" "${CONTAINER_HOME}/logs"
chown -R "${CONTAINER_USER}":"${CONTAINER_GROUP}" "${CONTAINER_HOME}/toprocess"

echo "Starting processflac..."
su "${CONTAINER_USER}" -s "/bin/sh" -c "cd && /usr/bin/processflac.sh"
