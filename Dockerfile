#################################################
# Authors
#   Coloc Malakof <colocmalakof@gmail.com>
# Description
#   This is an optimized processflac docker image.
# Documentation
#   https://gitlab.com/colocmalakof/processflac
#################################################

# Baseline
FROM alpine:edge

# Metadata
LABEL Maintainer="colocmalakof" \
    Version="1.0" \
    Description="Optimized processflac docker image."

# Environment variables for the container
ENV UID=1000 \
    GID=1000

# Configure the system
RUN set -xe && \
    # Add sources
    echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    # Install required packages
    apk add --no-cache \
        flac \
        imagemagick \
        jpegoptim \
        python3 \
        py3-pip && \
    pip3 install --no-cache \
        ruamel.yaml && \
    # Create home directory
    mkdir /processflac && \
    # Create symlinks
    ln -s /processflac/config /config && \
    ln -s /processflac/logs /logs && \
    ln -s /processflac/toprocess /toprocess

# Copy the files into the container
COPY build/processflac.sh /usr/bin/processflac.sh
COPY build/entrypoint.sh /usr/bin/entrypoint.sh
COPY build/etc /etc
COPY build/usr /usr

# Volumes
VOLUME [ "/config", "/logs", "/toprocess" ]

# Command to execute when the container is created
CMD [ "/usr/bin/entrypoint.sh" ]
